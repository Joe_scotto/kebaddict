<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap Mobile Optimization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Meta Tags -->
    <meta name="description" content="" />
    <title>Alex, the Kebaddict</title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png">
    <!-- Bootstrap CDN CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <?php require 'templates/tracking.php'; ?>
</head>

<body>
    
</body>
</html>