<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap Mobile Optimization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Meta Tags -->
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <title>Alex, the Kebaddict</title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png">
    <!-- Bootstrap CDN CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <?php require 'templates/tracking.php'; ?>
</head>

<body>
    <!-- Header -->
    <?php require "templates/header.php"; ?>    

    <?php if (Admin::checkLogin()) { ?>
    <div id="admin_add_post_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-3"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form method="post" action="" class="main_form" enctype="multipart/form-data">
                        <h1 class="add_post_header">New Post</h1>
                        <h1 class="logout pull-right">
                            <button type="submit" name="logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                            </button>
                        </h1>
                        <hr>
                        <!-- Location -->
                        <label Locationfor="admin_add_post_location">Location</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <span class="glyphicon glyphicon-globe"></span>
                            </span>
                            <input type="text" class="form-control" id="admin_add_post_location" placeholder="Location" name="admin_add_post_location" value="<?php echo htmlentities($_SESSION['data']['location']); ?>">
                        </div>
                        <br>
                        <!-- Rating -->
                        <label>Rating</label>
                        <div class="btn-group btn-group-justified ratings" data-toggle="buttons">
                            <label class="btn btn-primary <?php if ($_SESSION['data']['rating'] == '1') echo htmlentities('active'); ?>">
                                <input type="radio" name="admin_add_post_rating" id="1" value="1" <?php if ($_SESSION['data']['rating'] == '1') echo htmlentities('checked'); ?>>1
                            </label>
                            <label class="btn btn-primary <?php if ($_SESSION['data']['rating'] == '2') echo htmlentities('active'); ?>">
                                <input type="radio" name="admin_add_post_rating" id="2" value="2" <?php if ($_SESSION['data']['rating'] == '2') echo htmlentities('checked'); ?>>2
                            </label>
                            <label class="btn btn-primary <?php if ($_SESSION['data']['rating'] == '3') echo htmlentities('active'); ?>">
                                <input type="radio" name="admin_add_post_rating" id="3" value="3" <?php if ($_SESSION['data']['rating'] == '3') echo htmlentities('checked'); ?>>3
                            </label>
                            <label class="btn btn-primary <?php if ($_SESSION['data']['rating'] == '4') echo htmlentities('active'); ?>">
                                <input type="radio" name="admin_add_post_rating" id="4" value="4" <?php if ($_SESSION['data']['rating'] == '4') echo htmlentities('checked'); ?>>4
                            </label>
                            <label class="btn btn-primary <?php if ($_SESSION['data']['rating'] == '5') echo htmlentities('active'); ?>">
                                <input type="radio" name="admin_add_post_rating" id="5" value="5" <?php if ($_SESSION['data']['rating'] == '5') echo htmlentities('checked'); ?>>5
                            </label>
                        </div>
                        <!-- Image -->
                        <div id="select_image_container">
                            <label for="admin_add_post_image">Image</label>
                            <label for="admin_add_post_image" id="select_image_label" onchange="checkFile()" data-toggle="tooltip" data-placement="bottom" title='Verify image has an aspect ratio of "3:2"'>Select Image</label>
                            <input type="file" name="admin_add_post_image" id="admin_add_post_image" accept="image/jpg, image/jpeg">
                        </div>
                        <hr>
                        <!-- English -->
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 english">
                            <h1>English</h1>
                            <hr>
                            <!-- Title -->
                            <label Locationfor="admin_add_post_title_en">Title</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <span class="glyphicon glyphicon-text-background"></span>
                                </span>
                                <input type="text" class="form-control" id="admin_add_post_title_en" placeholder="Title" name="admin_add_post_title_en" value="<?php echo htmlentities($_SESSION['data']['title_en']); ?>">
                            </div>
                            <!-- Article -->
                            <div class="form-group">
                                <label for="admin_add_post_article_en" class="marginLabel">Article</label>
                                <textarea name="admin_add_post_article_en" id="admin_add_post_article_en" class="form-control" rows="5"><?php echo htmlentities($_SESSION['data']['article_en']); ?></textarea>
                            </div>  
                        </div>
                        <!-- Spanish -->
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 spanish">
                            <h1>Spanish</h1>
                            <hr>
                            <!-- Title -->
                            <label Locationfor="admin_add_post_title_es">Title</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <span class="glyphicon glyphicon-text-background"></span>
                                </span>
                                <input type="text" class="form-control" id="admin_add_post_title_es" placeholder="Title" name="admin_add_post_title_es" value="<?php echo htmlentities($_SESSION['data']['title_es']); ?>">
                            </div>
                            <!-- Article -->
                            <div class="form-group">
                                <label for="admin_add_post_article_es" class="marginLabel">Article</label>
                                <textarea name="admin_add_post_article_es" class="form-control" rows="5"><?php echo htmlentities($_SESSION['data']['article_es']); ?></textarea>
                            </div>  
                        </div>
                        <input type="submit" name="admin_add_post_submit" value="Add Post">
                    </form>
                </div>
                <div class="col-lg-4 col-md-3"></div>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div id="admin_login_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-3"></div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <form method="post" action="">
                        <h1>Admin Login</h1>
                        <hr>
                        <!-- Username -->
                        <label for="admin_login_username">Username</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <span class="glyphicon glyphicon-user"></span>
                            </span>
                            <input type="text" class="form-control" id="admin_login_username" placeholder="Username" name="admin_login_username" value="<?php echo htmlentities($_POST['admin_login_username']); ?>">
                        </div>
                        <!-- Username -->
                        <label for="admin_login_password">Password</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>
                            <input type="password" class="form-control" id="admin_login_password" placeholder="Password" name="admin_login_password" value="<?php echo htmlentities($_POST['admin_login_password']); ?>">
                        </div>
                        <input type="submit" name="admin_login_submit" value="Login">
                    </form>
                </div>
                <div class="col-lg-4 col-md-3"></div>
            </div>
        </div>
    </div>
    <?php }?>

    <!-- Footer -->
    <?php require "templates/footer.php"; ?>

    <script>

        function checkFile () {  
            if(document.getElementById("admin_add_post_image").value != "") {
                document.getElementById("select_image_label").style.backgroundColor = "#66CC99";
            } else {
                document.getElementById("select_image_label").style.backgroundColor = "#7ea4f5";
            }
        }

        $(document).ready(function(){
            $("#admin_add_post_image").on('change',function(){
               if(document.getElementById("admin_add_post_image").value != "") {
                    document.getElementById("select_image_label").style.backgroundColor = "#66CC99";
                } else {
                    document.getElementById("select_image_label").style.backgroundColor = "#7ea4f5";
                }
            });
        });

    </script>
</body>
</html>