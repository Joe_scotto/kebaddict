<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap Mobile Optimization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Meta Tags -->
    <meta name="description" content="" />
    <title>Alex, the Kebaddict - <?php echo htmlentities($postInfo['location']); ?></title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png">
    <!-- Bootstrap CDN CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <?php require 'templates/tracking.php'; ?>
</head>

<body>
    <!-- Header -->
    <?php require "templates/header.php"; ?>    
        
    <?php if ($_SESSION['logged_in'] == 1) { ?>
        <!-- Edit Modal -->
        <div class="modal fade" tabindex="-1" role="dialog" id="edit_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Article</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="" class="main_form" enctype="multipart/form-data">
                            <!-- Location -->
                            <label Locationfor="admin_add_post_location">Location</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <span class="glyphicon glyphicon-globe"></span>
                                </span>
                                <input type="text" class="form-control" id="admin_add_post_location" placeholder="Location" name="edit_post_location" value="<?php echo htmlentities($postInfo['location']); ?>">
                            </div>
                            <br>
                            <!-- Rating -->
                            <label>Rating</label>
                            <div class="btn-group btn-group-justified ratings" data-toggle="buttons">
                                <label class="btn btn-primary <?php if ($postInfo['rating'] == '1') echo htmlentities('active'); ?>">
                                    <input type="radio" name="edit_post_rating" id="1" value="1" <?php if ($postInfo['rating'] == '1') echo htmlentities('checked'); ?>>1
                                </label>
                                <label class="btn btn-primary <?php if ($postInfo['rating'] == '2') echo htmlentities('active'); ?>">
                                    <input type="radio" name="edit_post_rating" id="2" value="2" <?php if ($postInfo['rating'] == '2') echo htmlentities('checked'); ?>>2
                                </label>
                                <label class="btn btn-primary <?php if ($postInfo['rating'] == '3') echo htmlentities('active'); ?>">
                                    <input type="radio" name="edit_post_rating" id="3" value="3" <?php if ($postInfo['rating'] == '3') echo htmlentities('checked'); ?>>3
                                </label>
                                <label class="btn btn-primary <?php if ($postInfo['rating'] == '4') echo htmlentities('active'); ?>">
                                    <input type="radio" name="edit_post_rating" id="4" value="4" <?php if ($postInfo['rating'] == '4') echo htmlentities('checked'); ?>>4
                                </label>
                                <label class="btn btn-primary <?php if ($postInfo['rating'] == '5') echo htmlentities('active'); ?>">
                                    <input type="radio" name="edit_post_rating" id="5" value="5" <?php if ($postInfo['rating'] == '5') echo htmlentities('checked'); ?>>5
                                </label>
                            </div>
                            <!-- English -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 english">
                                <h1>English</h1>
                                <hr>
                                <!-- Title -->
                                <label Locationfor="admin_add_post_title_en">Title</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <span class="glyphicon glyphicon-text-background"></span>
                                    </span>
                                    <input type="text" class="form-control" id="admin_add_post_title_en" placeholder="Title" name="edit_post_title_en" value="<?php echo htmlentities($postInfo['title_en']); ?>">
                                </div>
                                <!-- Article -->
                                <div class="form-group">
                                    <label for="admin_add_post_article_en" class="marginLabel">Article</label>
                                    <textarea name="edit_post_article_en" id="admin_add_post_article_en" class="form-control" rows="5"><?php echo htmlentities($postInfo['article_en']); ?></textarea>
                                </div>  
                            </div>
                            <!-- Spanish -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 spanish">
                                <h1>Spanish</h1>
                                <hr>
                                <!-- Title -->
                                <label Locationfor="admin_add_post_title_es">Title</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <span class="glyphicon glyphicon-text-background"></span>
                                    </span>
                                    <input type="text" class="form-control" id="admin_add_post_title_es" placeholder="Title" name="edit_post_title_es" value="<?php echo htmlentities($postInfo['title_es']); ?>">
                                </div>
                                <!-- Article -->
                                <div class="form-group">
                                    <label for="admin_add_post_article_es" class="marginLabel">Article</label>
                                    <textarea name="edit_post_article_es" class="form-control" rows="5"><?php echo htmlentities($postInfo['article_es']); ?></textarea>
                                </div>  
                            </div>
                        
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="edit_post_submit" value="Save Changes">
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Delete Modal -->
        <div class="modal fade delete_modal" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Are you sure?</h4>
                    </div>
                    <div class="modal-body">
                        <p>You have selected to delete this post.</p>
                        <p>Once a post is deleted, it is gone from our servers forever. Are you absolutely certain that you want to delete this post?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <form method="post" action="" style="display: inline-block;">
                            <input type="submit" class="btn btn-danger" id="delete_post" name="delete_post" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Hero -->
    <div id="article_hero_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1><?php echo htmlentities($postInfo['location']); ?></h1>
                    <p><?php Article::listRating($_GET['id']); ?></p> 
                </div>
            </div>  
        </div>
    </div>
    
    <div id="article_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="content">
                    <?php if ($_SESSION['logged_in'] == 1) { ?>   
                    <a data-toggle="modal" data-target="#edit_modal">Edit</a>
                    -
                    <a data-toggle="modal" data-target="#delete_modal">Delete</a>
                    <?php } ?>
                        <img src="<?php echo htmlentities($postInfo['image']); ?>/image-full.jpg" class="img-responsive">
                        <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                            <h1><?php echo htmlentities($postInfo['title_es']); ?></h1>
                            <h1 class="pull-right"><?php echo htmlentities(date('j/m/Y', strtotime($postInfo['date_added']))); ?></h1>
                            <hr>
                            <p><?php echo nl2br(htmlentities($postInfo['article_es'])); ?></p>
                        <?php } else if ($_GET['lang'] == 'en') { ?>
                            <h1><?php echo htmlentities($postInfo['title_en']); ?></h1>
                            <h1 class="pull-right"><?php echo htmlentities(date( 'F d, Y', strtotime($postInfo['date_added']))); ?></h1>
                            <hr>
                            <p><?php echo nl2br(htmlentities($postInfo['article_en'])); ?></p>
                        <?php } else { ?>
                            <h1><?php echo htmlentities($postInfo['title_es']); ?></h1>
                            <h1 class="pull-right"><?php echo htmlentities(date('j/m/Y', strtotime($postInfo['date_added']))); ?></h1>
                            <hr>
                            <p><?php echo nl2br(htmlentities($postInfo['article_es'])); ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <?php require "templates/footer.php"; ?>
</body>
</html>