<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<h1>©2016 Kebaddict</h1>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<p>A <a href="https://twitter.com/joe_scotto">Joe Scotto</a> Product.</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<ul>
						<a href="https://instagram.com/kebaddict">
							<li><img src="images/instagram.png"></li>
						</a>
						<a href="https://www.facebook.com/kebaddict">
							<li><img src="images/facebook.png"></li>
						</a>
						<a href="<?php echo Utilities::appendVariableToUrl('lang', 'es'); ?>">
							<li><img src="images/es.png"></li>
						</a>
						<a href="<?php echo Utilities::appendVariableToUrl('lang', 'en'); ?>">
							<li><img src="images/en.png"></li>
						</a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- jQuery CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- Bootstrap CDN JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- Main Javascript File -->
<script src="script/main.js"></script>


<!-- Show error modal if there is an error -->
<?php if (isset($_SESSION['message'])) {?>
<script>
$(document).ready(function (e) {
	$("#message_modal").modal("show");
});
</script>
<?php } ?>
