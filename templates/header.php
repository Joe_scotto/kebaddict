<!-- Contact Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="contact_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        Contacta
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        Contact
                    <?php } else { ?>
                        Contacta
                    <?php } ?>
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        <!-- Contact Name -->
                        <div class="form-group">
                            <label for="contact_name">Nombre</label>
                            <input type="text" class="form-control" id="contact_name" placeholder="Nombre" name="contact_name" value="<?php echo htmlentities($_POST['contact_name']); ?>">
                        </div>
                        <!-- Contact Email -->
                        <div class="form-group">
                            <label for="contact_email">Email</label>
                            <input type="text" class="form-control" id="contact_email" placeholder="Email" name="contact_email" value="<?php echo htmlentities($_POST['contact_email']); ?>">
                        </div>
                        <!-- Contact Subject -->
                        <div class="form-group">
                            <label for="contact_subject">Asunto</label>
                            <input type="text" class="form-control" id="contact_subject" placeholder="Asunto" name="contact_subject" value="<?php echo htmlentities($_POST['contact_subject']); ?>">
                        </div>
                        <!-- Contact Message -->
                        <div class="form-group">
                            <label for="contact_message">Mensaje</label>
                            <textarea class="form-control" id="contact_message" placeholder="Mensaje" name="contact_message"><?php echo htmlentities($_POST['contact_message']); ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="contact_submit">Enviar</button>
                        </div>
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        <!-- Contact Name -->
                        <div class="form-group">
                            <label for="contact_name">Name</label>
                            <input type="text" class="form-control" id="contact_name" placeholder="Name" name="contact_name" value="<?php echo htmlentities($_POST['contact_name']); ?>">
                        </div>
                        <!-- Contact Email -->
                        <div class="form-group">
                            <label for="contact_email">Email</label>
                            <input type="text" class="form-control" id="contact_email" placeholder="Email" name="contact_email" value="<?php echo htmlentities($_POST['contact_email']); ?>">
                        </div>
                        <!-- Contact Subject -->
                        <div class="form-group">
                            <label for="contact_subject">Subject</label>
                            <input type="text" class="form-control" id="contact_subject" placeholder="Subject" name="contact_subject" value="<?php echo htmlentities($_POST['contact_subject']); ?>">
                        </div>
                        <!-- Contact Message -->
                        <div class="form-group">
                            <label for="contact_message">Message</label>
                            <textarea class="form-control" id="contact_message" placeholder="Message" name="contact_message"><?php echo htmlentities($_POST['contact_message']); ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" name="contact_submit">Send</button>
                        </div>
                    <?php } else { ?>
                        <!-- Contact Name -->
                        <div class="form-group">
                            <label for="contact_name">Nombre</label>
                            <input type="text" class="form-control" id="contact_name" placeholder="Nombre" name="contact_name" value="<?php echo htmlentities($_POST['contact_name']); ?>">
                        </div>
                        <!-- Contact Email -->
                        <div class="form-group">
                            <label for="contact_email">Email</label>
                            <input type="text" class="form-control" id="contact_email" placeholder="Email" name="contact_email" value="<?php echo htmlentities($_POST['contact_email']); ?>">
                        </div>
                        <!-- Contact Subject -->
                        <div class="form-group">
                            <label for="contact_subject">Asunto</label>
                            <input type="text" class="form-control" id="contact_subject" placeholder="Asunto" name="contact_subject" value="<?php echo htmlentities($_POST['contact_subject']); ?>">
                        </div>
                        <!-- Contact Message -->
                        <div class="form-group">
                            <label for="contact_message">Mensaje</label>
                            <textarea class="form-control" id="contact_message" placeholder="Mensaje" name="contact_message"><?php echo htmlentities($_POST['contact_message']); ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="contact_submit">Enviar</button>
                        </div>
                    <?php } ?>
                </form> 
            </div>
        </div>
    </div>
</div>

<!-- About Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="about_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        Acerca de Kebaddict
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        About
                    <?php } else { ?>
                        Acerca de Kebaddict
                    <?php } ?>
                    About
                </h4>
            </div>
            <div class="modal-body">
                <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                    <p>Esta historia de amor comenzó en la primavera de 2014. Sólo había estado viviendo en España durante unos dos meses en el momento y un par de amigos me invitaron a ir a comer un kebab después de la escuela. </ P>
                    <p>Decidimos ir a @universokekab y tan pronto como llegamos allí nos dimos cuenta el aire acondicionado no estaba encendido. Estaba caliente, olía raro y la gente nos mira un poco divertido. No pensé mucho por ella y recuerdo sentado en el asiento de la esquina y sólo tener un buen tiempo y la espera para nuestra comida para llegar allí. Después de una espera de 5 minutos, mi primera donner kebab se colocó frente a mí. Entré y le dio un mordisco.</p>
                    <p>Al instante me puse a hacer caso omiso de mi entorno: la música de mierda, las miradas extrañas, el olor extraño, incluso mis amigos. En ese momento, nada más importaba. Fue amor a primera gusto. Salto hacia adelante de un par de años y las horas extraordinarias que comer un kebab no puedo dejar de pensar "Esto le da sentido a mi vida".</p>
                <?php } else if ($_GET['lang'] == 'en') { ?>
                    <p>This love story commenced in the spring 2014. I had only been living in Spain for about two months at the time and a couple of friends invited me to go eat a kebab after school.</p>
                    <p>We decided to go to @universokekab and as soon as we got there we realized the ac wasn’t on. It was hot, it smelled weird and the people there look at us kinda funny. I didn’t think much by it and I remember sitting at the corner seat and just having a good time and waiting for our food to get there. After a 5 minute wait, my first donner kebab was placed in front of me. I went in and took a bite.</p>
                    <p>I instantly began to ignore my surroundings: the shitty music, the weird looks, the odd smell, even my friends. At that point, nothing else mattered. It was love on first taste. Skip forward a couple of years and overtime I eat a kebab I can’t help but to think “This gives my life meaning”.</p>
                <?php } else { ?>
                    <p>Esta historia de amor comenzó en la primavera de 2014. Sólo había estado viviendo en España durante unos dos meses en el momento y un par de amigos me invitaron a ir a comer un kebab después de la escuela. </ P>
                    <p>Decidimos ir a @universokekab y tan pronto como llegamos allí nos dimos cuenta el aire acondicionado no estaba encendido. Estaba caliente, olía raro y la gente nos mira un poco divertido. No pensé mucho por ella y recuerdo sentado en el asiento de la esquina y sólo tener un buen tiempo y la espera para nuestra comida para llegar allí. Después de una espera de 5 minutos, mi primera donner kebab se colocó frente a mí. Entré y le dio un mordisco.</p>
                    <p>Al instante me puse a hacer caso omiso de mi entorno: la música de mierda, las miradas extrañas, el olor extraño, incluso mis amigos. En ese momento, nada más importaba. Fue amor a primera gusto. Salto hacia adelante de un par de años y las horas extraordinarias que comer un kebab no puedo dejar de pensar "Esto le da sentido a mi vida".</p>
                <?php } ?>
                <div class="modal-footer">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <?php } else { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Message Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="message_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        <?php echo htmlentities($_SESSION['message']['title_es']); ?>
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        <?php echo htmlentities($_SESSION['message']['title_en']); ?>
                    <?php } else { ?>
                        <?php echo htmlentities($_SESSION['message']['title_es']); ?>
                    <?php } ?>
                </h4>
            </div>
            <div class="modal-body">
                <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                    <?php echo htmlentities($_SESSION['message']['message_es']); ?>
                <?php } else if ($_GET['lang'] == 'en') { ?>
                    <?php echo htmlentities($_SESSION['message']['message_en']); ?>
                <?php } else { ?>
                    <?php echo htmlentities($_SESSION['message']['message_es']); ?>
                <?php } ?>
                <div class="modal-footer">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <?php } else { ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php if (isset($_GET['lang'])) { ?>
                        <a class="navbar-brand" href="index.php?lang=<?php echo $_GET['lang']; ?>">Kebaddict</a>
                    <?php } else { ?>
                        <a class="navbar-brand" href="index.php">Kebaddict</a>
                    <?php } ?>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                            <li><a data-toggle="modal" data-target="#about_modal">Acerca de Kebaddict</a></li>
                            <li><a  data-toggle="modal" data-target="#contact_modal">Contacta</a></li>
                            <form method="post" action="" class="navbar-form navbar-left" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Búsqueda" name="search_term">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="search_submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        <?php } else if ($_GET['lang'] == 'en') { ?>
                            <li><a data-toggle="modal" data-target="#about_modal">About</a></li>
                            <li><a  data-toggle="modal" data-target="#contact_modal">Contact</a></li>
                            <form method="post" action="" class="navbar-form navbar-left" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search" name="search_term">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="search_submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        <?php } else { ?>
                            <li><a data-toggle="modal" data-target="#about_modal">Acerca de Kebaddict</a></li>
                            <li><a  data-toggle="modal" data-target="#contact_modal">Contacta</a></li>
                            <form method="post" action="" class="navbar-form navbar-left" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Búsqueda" name="search_term">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="search_submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>