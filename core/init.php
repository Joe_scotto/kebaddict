<?php
 
//Start Sessions
session_start();

//Test Error Reporting
error_reporting(E_ALL ^ E_NOTICE);

//Autoload Classes
spl_autoload_register(function ($class) {
	require 'classes/' . $class . '.php';
});

//Unset Message 
if (isset($_COOKIE['sessionPersist'])) {
	if ($_COOKIE['sessionPersist'] == 1) {
		//Prepare cookie to unset on next reload
		setcookie('sessionPersist', $_COOKIE['sessionPersist'] + 1);
	} else if ($_COOKIE['sessionPersist'] == 2) {
		unset($_SESSION['message']);

		//Unset Session Persistence Cookie
		setcookie('sessionPersist', 0);
	} else if ($_COOKIE['sessionPersist'] == 0) {
		//Unset Session Persistence Cookie
		setcookie('sessionPersist', "", time()-1);
	}
} else if (!isset($_COOKIE['sessionPersist']) || $_COOKIE['sessionPersist'] == 0) {
	unset($_SESSION['message']);

	//Unset Session Persistence Cookie
	unset($_COOKIE['sessionPersist']);
}

//Configuration
$GLOBALS['config'] = array(
	'mysql' => array(
		'user' => 'joe_scotto',
		'pass' => 'Dillinger1',
		'host' => 'database.kebaddict.com',
		'db' => 'kebaddict'
	)
);

//Global Variables
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$postInfo = Article::grabPost($_GET['id']);

//Initiate Search
if (isset($_POST['search_submit'])) {
	if (!empty($_POST['search_term'])) {
		header("Location: search.php?search_term=" . $_POST['search_term']);
	}
}

//Initiate Admin Login 
if (isset($_POST['admin_login_submit'])) {
	if (!empty($_POST['admin_login_username']) && !empty($_POST['admin_login_password'])) {
		Admin::Login($_POST['admin_login_username'], $_POST['admin_login_password']);
	} else {
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Error",
			'message_en' => "Please fill in all fields.",
			'title_es' => "Error",
			'message_es' => "Por favor rellene todos los campos.",
		);
		header("Location: " . $url);
	}
}

//Initiate Admin Add Post
if (isset($_POST['admin_add_post_submit'])) {
	if (!empty($_POST['admin_add_post_location']) && 
		!empty($_POST['admin_add_post_rating']) && 
		!empty($_FILES['admin_add_post_image']['name']) &&
		!empty($_POST['admin_add_post_title_en']) && 
		!empty($_POST['admin_add_post_article_en']) && 
		!empty($_POST['admin_add_post_title_es']) && 
		!empty($_POST['admin_add_post_article_es'])) {
		Admin::addPost($_POST);
	} else {
		$_SESSION['data'] = array(
			'location' => $_POST['admin_add_post_location'],
			'rating' => $_POST['admin_add_post_rating'],
			'title_en' => $_POST['admin_add_post_title_en'],
			'article_en' => $_POST['admin_add_post_article_en'],
			'title_es' => $_POST['admin_add_post_title_es'],
			'article_es' => $_POST['admin_add_post_article_es']
		);
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Error",
			'message_en' => "Please fill in all fields.",
			'title_es' => "Error",
			'message_es' => "Por favor rellene todos los campos.",
		);
		header("Location: " . $url);
	} 
}

//Initiate Edit Post
if (isset($_POST['edit_post_submit'])) {
	if (!empty($_POST['edit_post_location']) && !empty($_POST['edit_post_title_en']) && !empty($_POST['edit_post_article_en']) && !empty($_POST['edit_post_title_es']) && !empty($_POST['edit_post_article_es'])) {
		Admin::editPost($_POST, $_GET['id']);
	} else {
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Error",
			'message_en' => "A field was empty, All fields must contain data. Please try again.",
			'title_es' => "Error",
			'message_es' => "parece que has dejado un campo en blanco. Todos los campos deben de tener información. Por favor inténtalo de nuevo.",
		);
		header("Location: " . $url);
	}
}

//Delete Post {
 if (isset($_POST['delete_post'])) {
 	Admin::deletePost($_GET['id']);
 }

//Logout
if (isset($_POST['logout'])) {
	Admin::logout();
}

//Initiate Mail
if (isset($_POST['contact_submit'])) {
	if (!empty($_POST['contact_name']) && !empty($_POST['contact_email']) && !empty($_POST['contact_subject']) && !empty($_POST['contact_message'])) {
		Mail::send($_POST, $url);
	} else {
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Error",
			'message_en' => "Please fill in all fields.",
			'title_es' => "Error",
			'message_es' => "Por favor rellene todos los campos.",
		);
		header("Location: " . $url);
	}
}