<?php

class Admin {
	/**
	 * Logs the user in
	 * @param string $username Username of the user
	 * @param string $password Password of the user
	 */
	public static function Login ($username, $password) {
		//Database Connection 
		$db = Database::getInstance();

		//Grab row where username matches
		$queryUsername = "SELECT * FROM users WHERE username = :username";
		$queryUsernamePrepare = $db->getConnection()->prepare($queryUsername);
		$queryUsernamePrepare->execute(array(
			':username' => $username
		));

		//Return Results
		$queryUsernameResults = $queryUsernamePrepare->fetch(PDO::FETCH_ASSOC);

		//Validate Password 
		if (password_verify($password, $queryUsernameResults['password'])) {
			//Login User
			$_SESSION['logged_in'] = 1;
			header("Location: admin.php");
		} else {
			//Return Error Message
		}
	}

	/**
	 * Checks if the user is logged in or not
	 * @return bool True if logged in, false if not
	 */
	public static function checkLogin () {
		//If session is found, allow user. Otherwise block access
		if (isset($_SESSION['logged_in'])) {
			return true;
		} else {
			return false;
			die();
		}
	}

	/**
	 * Logs out the current user
	 * @return resource Redirects to homepage after unsetting session
	 */
	public static function logout () {
		//Unset Session
		unset($_SESSION['logged_in']);

		//Redirect to homepage
		header("Location: index.php");
	}

	/**
	 * Adds a new post
	 * @param mixed $data $_POST Data
	 */
	public static function addPost ($data) {
		//Database Connection
		$db = Database::getInstance();

		//Variables
		$id = time();
		$image = $_FILES['admin_add_post_image']['name'];
		$extension = pathinfo($image, PATHINFO_EXTENSION);
		$directory = "posts/" . $id;

		//Insert into database
		$query = "INSERT INTO posts (location, rating, image, title_en, article_en, title_es, article_es, date_added, unique_id) 
							 VALUES(:location, :rating, :image, :title_en, :article_en, :title_es, :article_es, NOW(), :unique_id)";
		$queryPrepare = $db->getConnection()->prepare($query);
		$queryPrepare->execute(array(
			':location' => $data['admin_add_post_location'],
			':rating' => $data['admin_add_post_rating'],
			':image' => $directory . "/",
			':title_en' => $data['admin_add_post_title_en'],
			':article_en' => $data['admin_add_post_article_en'],
			':title_es' => $data['admin_add_post_title_es'],
			':article_es' => $data['admin_add_post_article_es'],
			':unique_id' => $id
		));

		//Create directory 
		mkdir('posts/' . $id);

		//Upload Image
		if (move_uploaded_file($_FILES['admin_add_post_image']['tmp_name'], $directory . "/" . $image)) {
			self::uploadImage($directory, $image);
		}
		
		//Redirect to post
		header("Location: article.php?id=" . $id);
	}

	/**
	 * Uploads the image
	 * @param  string $directory Where the image files are located
	 * @return resource Removes original file after upload
	 */
	public static function uploadImage ($directory, $image) {
		$counter = 1;
		while ($counter <= 2) {
			$params = array(
				'width' => array(
					1 => 1920,
					2 => 768
				),
				'height' => array(
					1 => 1280,
					2 => 512
				),
				'filename' => array(
					1 => "image-full",
					2 => "image-thumbnail"
				),
				'quality' => array(
					1 => 60,
					2 => 80
				)
 			);

			//Initialize / load image
			$resizeObj = new Resize($directory . "/" . $image);
			 
			//Resize image (options: exact, portrait, landscape, auto, crop)
			$resizeObj->resizeImage($params['width'][$counter], $params['height'][$counter]);
			 
			//Save image
			$resizeObj->saveImage($directory . "/" . $params['filename'][$counter] . '.jpg', $params['quality'][$counter]);

			$counter++;
		}

		//Remove File 
		unlink($directory . "/" . $image);
	}

	/**
	 * Edits a post
	 * @param  mixed $data $_POST Data
	 * @return bool Redirects upon success.
	 */
	public static function editPost ($data, $id) {
		//Database Connection
		$db = Database::getInstance();

		//Insert into database
			$query = "UPDATE `posts` SET location = :location, 
									   rating = :rating, 
									   title_en = :title_en, 
									   article_en = :article_en, 
									   title_es = :title_es, 
									   article_es = :article_es 
									   WHERE unique_id = :id";
		$queryPrepare = $db->getConnection()->prepare($query);
		$queryPrepare->execute(array(
			':location' => $data['edit_post_location'],
			':rating' => $data['edit_post_rating'],
			':title_en' => $data['edit_post_title_en'],
			':article_en' => $data['edit_post_article_en'],
			':title_es' => $data['edit_post_title_es'],
			':article_es' => $data['edit_post_article_es'],
			':id' => $id
		));

		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Success",
			'message_en' => "Post edit was successful.",
			'title_es' => "Success",
			'message_es' => "Post edit was successful.",
		);

		header("Location: article.php?id=" . $id);
	}

	public static function deletePost ($id) {
		//Database Connection
		$db = Database::getInstance();

		//Delete coil from database
		$query = $db->getConnection()->prepare("DELETE FROM posts WHERE unique_id = :id");
		$query->execute(array(
			':id' => $id
		));

		//Delete files
		//unlink("posts/" . $id);

		//Redirect
		header("Location: index.php");
		exit();
	}
}