<?php

class Database {
	private static $_instance;
	public $pdo;

	/**
	 * Singleton for database connection
	 * @return class Returns one instance of the Database class
	 */
	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Connects to database
	 * @return mysql PDO connection to mysql databse
	 */
	public function getConnection() {
		try {
			//Attempt to connect
			$this->pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/user'), Config::get('mysql/pass'));

			//Return connection
			return $this->pdo;
		} catch (PDOException $e) {
			throw new PDOException($e);
		}
	}
}

