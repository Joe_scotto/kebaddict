<?php

class Article {
	/**
	 * Grabs the data for a specified id
	 * @param  string $id Unique id of the current post
	 * @return mixed Array containing data
	 */
	public static function grabPost ($id) {
		//Database Connection
		$db = Database::getInstance();

		//Select post from database
		$query = "SELECT * FROM posts WHERE unique_id = :unique_id";
		$queryPrepare = $db->getConnection()->prepare($query);
		$queryPrepare->execute(array(
			':unique_id' => $id
		));

		//Return results
		return $queryPrepare->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Formats ratings
	 * @param  string $id Unique id of the current post
	 * @return string Formatted image strings for rating
	 */
	public static function listRating ($id) {
		//Variables
		$rating = self::grabPost($id)['rating'];
		$yellowStars = $rating;
		$counterYellow = 1;
		$greyStars = 5 - $rating;
		$counterGrey = 1;

		//Yellow Stars
		while ($counterYellow <= $yellowStars) {
			echo '<img src="images/star-yellow.png" alt="">';
			$counterYellow++;
		}

		//Grey Stars
		while ($counterGrey <= $greyStars) {
			echo '<img src="images/star-grey.png" alt="">';
			$counterGrey++;
		}
	}

	/**
	 * Grabs all posts from the database
	 * @return mixed Array containing all posts
	 */
	public static function grabAllPosts ($sorting = null, $page = 1) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
        $page = isset($page) ? (int)$page : 1;
        $perPage = 10;

        //Positioning
        $start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Select all records
		switch ($sorting) {
			//No value determined by user
			case null:
				$query = "SELECT * FROM posts ORDER BY date_added DESC LIMIT {$start}, {$perPage}";
				break;
			//Newest
			case 'newest':
				$query = "SELECT * FROM posts ORDER BY date_added DESC LIMIT {$start}, {$perPage}";
				break;
			//Oldest
			case 'oldest':
				$query = "SELECT * FROM posts ORDER BY date_added ASC LIMIT {$start}, {$perPage}";
				break;
			case 'highest': 
                $query = "SELECT * FROM `posts` ORDER BY rating DESC LIMIT {$start}, {$perPage}";
                break;
            case 'lowest': 
                $query = "SELECT * FROM `posts` ORDER BY rating ASC LIMIT {$start}, {$perPage}";
                break;
			default:
				$query = "SELECT * FROM posts";
				break;
		}

		$queryPrepare = $db->getConnection()->prepare($query);
		$queryPrepare->execute();

		//Return Results
		return $queryPrepare;
	}

	public static function getNumericalNumberOfPosts () {
        //Database Connection
        $db = Database::getInstance();

        //Select all rows from database where $coil_id matches
        $query = $db->getConnection()->prepare("SELECT id FROM `posts`");
        $query->execute();

        //Number of rows
        return $query->rowCount();
    }

	/**
     * Pagination for search 
     * @param  string $search_term What to search for
     * @param  int $page Current page
     * @return int Number of pages
     */
    public static function listPagination ($page = 1) {
        //Database Connection
        $db = Database::getInstance();

        //User Input
        $page = isset($page) ? (int)$page : 1;
        $perPage = 10;

        //Set page to one if lower
        $start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        //Query to get total
        $totalQuery = $db->getConnection()->prepare("SELECT id FROM `posts`");
        $totalQuery->execute();

        //Total number of pages
        $total = $totalQuery->rowCount();
        $pages = ceil($total / $perPage);

        return $pages;
    }  
}