<?php

class Mail {
	/**
	 * Sends an email to contact@coilerz.com
	 * @param  mixed $data $_POST Data
	 * @param  string $url Link to post back to
	 * @return string Success Message
	 */
	public static function send ($data, $url) {
		//Send Email
		mail("alex@kebaddict.com", $data['contact_subject'], "From: " . $data['contact_name'] . "\n"  . "Email: " . $data['contact_email'] . "\n\n" . $data['contact_message'], "From: New Message <contact@kebaddict.com>");

		//Redirect to prevent double submission
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = array(
			'title_en' => "Success",
			'message_en' => "Your email has been sent, please allow up to 7 days for a response.",
			'title_es' => "Todo bien por aquí",
			'message_es' => "Tu mensaje ha sido enviado. Te intentare contestar lo antes posible!",
		);
		header("Location: " . $url);
	}
}