<?php

class Search {
    /**
     * Returns the coils based on the search term
     * @param  string $search_term What to search for
     * @return array Coils found for that search term
     */
    public static function listFoundPosts ($search_term, $page = 1, $sorting) {
        //Database Connection
        $db = Database::getInstance();

        //User Input
        $page = isset($page) ? (int)$page : 1;
        $perPage = 10;

        //Positioning
        $start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        //Select comments from database based on $coil_id
        switch ($sorting) {
            //No value determined by user
            case null:
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY id DESC LIMIT {$start}, {$perPage}";
                break;
            //Newest
            case 'newest':
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY id DESC LIMIT {$start}, {$perPage}";
                break;
            //Oldest
            case 'oldest':
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY id ASC LIMIT {$start}, {$perPage}";
                break;
            case 'highest': 
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY rating DESC LIMIT {$start}, {$perPage}";
                break;
            case 'lowest': 
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY rating ASC LIMIT {$start}, {$perPage}";
                break;
            default:
                $query = "SELECT * FROM `posts` WHERE location LIKE :search_term ORDER BY id DESC LIMIT {$start}, {$perPage}";
                break;
        }
        $queryPrep = $db->getConnection()->prepare($query);
        $queryPrep->execute(array(
            ':search_term' => '%' . $search_term . '%'
        ));

        //Return results
        return $queryPrep;
    }

    public static function getNumericalNumberOfPosts ($search_term) {
        //Database Connection
        $db = Database::getInstance();

        //Select all rows from database where $coil_id matches
        $query = $db->getConnection()->prepare("SELECT id FROM `posts` WHERE location LIKE :search_term");
        $query->execute(array(
            ':search_term' => '%' . $search_term . '%'
        ));

        //Number of rows
        return $query->rowCount();
    }

    /**
     * Pagination for search 
     * @param  string $search_term What to search for
     * @param  int $page Current page
     * @return int Number of pages
     */
    public static function listPagination ($search_term, $page = 1) {
        //Database Connection
        $db = Database::getInstance();

        //User Input
        $page = isset($page) ? (int)$page : 1;
        $perPage = 10;

        //Set page to one if lower
        $start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        //Query to get total
        $totalQuery = $db->getConnection()->prepare("SELECT id FROM `posts` WHERE location LIKE :search_term");
        $totalQuery->execute(array(
            ':search_term' => '%' . $search_term . '%'
        ));

        //Total number of pages
        $total = $totalQuery->rowCount();
        $pages = ceil($total / $perPage);

        return $pages;
    }   
}