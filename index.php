<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap Mobile Optimization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Meta Tags -->
    <meta name="description" content="" />
    <title>Alex, the Kebaddict</title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png">
    <!-- Bootstrap CDN CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <?php require 'templates/tracking.php'; ?>
</head>

<body>
    <!-- Header -->
    <?php require "templates/header.php"; ?>   

    <!-- Hero -->
    <div id="home_hero_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="images/alex.jpg" class="img-circle img-responsive">
                    <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                        <h1>Alex Melillo</h1>
                        <p>Reviewer de Kebabs</p>
                    <?php } else if ($_GET['lang'] == 'en') { ?>
                        <h1>Alex Melillo</h1>
                        <p>Professional Kebab Reviewer</p>
                    <?php } else { ?>
                        <h1>Alex Melillo</h1>
                        <p>Reviewer de Kebabs</p>
                    <?php } ?>
                </div>
            </div>  
        </div>
    </div>

    

    <!-- Articles -->
    <div id="home_articles">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 article_header">
                    <h1>
                        <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                            <?php echo htmlentities(Article::getNumericalNumberOfPosts()); ?> Reviews
                        <?php } else if ($_GET['lang'] == 'en') { ?>
                            <?php echo htmlentities(Article::getNumericalNumberOfPosts()); ?> Reviews
                        <?php } else { ?>
                            <?php echo htmlentities(Article::getNumericalNumberOfPosts()); ?> Reviews
                        <?php } ?>
                    </h1>
                    <div class="dropdown pull-right">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                                <?php if (!isset($_GET['sorting'])) { ?>
                                    Clasificar
                                <?php } else if ($_GET['sorting'] == "newest") { ?>
                                    Más nuevo
                                <?php } else if ($_GET['sorting'] == "oldest") { ?>
                                    Más viejo 
                                <?php } else if ($_GET['sorting'] == "highest") { ?>
                                    Mejor nota
                                <?php } else if ($_GET['sorting'] == "lowest") { ?>
                                    Peor nota
                                <?php } ?>
                            <?php } else if ($_GET['lang'] == 'en') { ?>
                                <?php if (!isset($_GET['sorting'])) { ?>
                                    Sorting
                                <?php } else if ($_GET['sorting'] == "newest") { ?>
                                    Newest
                                <?php } else if ($_GET['sorting'] == "oldest") { ?>
                                    Oldest
                                <?php } else if ($_GET['sorting'] == "highest") { ?>
                                    Highest
                                <?php } else if ($_GET['sorting'] == "lowest") { ?>
                                    Lowest
                                <?php } ?>
                            <?php } else { ?>
                                <?php if (!isset($_GET['sorting'])) { ?>
                                    Clasificar
                                <?php } else if ($_GET['sorting'] == "newest") { ?>
                                    Más nuevo
                                <?php } else if ($_GET['sorting'] == "oldest") { ?>
                                    Más viejo 
                                <?php } else if ($_GET['sorting'] == "highest") { ?>
                                    Mejor nota
                                <?php } else if ($_GET['sorting'] == "lowest") { ?>
                                    Peor nota
                                <?php } ?>
                            <?php } ?>
                            
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'newest'); ?>">Más nuevo</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'oldest'); ?>">Más viejo</a></li>
                                <li><a href="?<?php echo Utilities::appendVariableToUrl('sorting', 'highest'); ?>">Más viejo</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'lowest'); ?>">Peor nota</a></li>
                            <?php } else if ($_GET['lang'] == 'en') { ?>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'newest'); ?>">Newest</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'oldest'); ?>">Oldest</a></li>
                                <li><a href="?<?php echo Utilities::appendVariableToUrl('sorting', 'highest'); ?>">Highest</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'lowest'); ?>">Lowest</a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'newest'); ?>">Más nuevo</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'oldest'); ?>">Más viejo</a></li>
                                <li><a href="?<?php echo Utilities::appendVariableToUrl('sorting', 'highest'); ?>">Más viejo</a></li>
                                <li><a href="<?php echo Utilities::appendVariableToUrl('sorting', 'lowest'); ?>">Peor nota</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <hr>
                </div> 
            </div>
        </div>
        <?php foreach (Article::grabAllPosts($_GET['sorting'], $_GET['page']) as $row) { ?>
        <div class="article_container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a href="article.php?id=<?php echo htmlentities($row['unique_id']); ?>">
                            <div class="article">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <img src="<?php echo htmlentities($row['image']); ?>/image-thumbnail.jpg" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text">
                                    <div>
                                        <?php if ($_GET['lang'] == 'es' || !isset($_GET['lang'])) { ?>
                                            <h1><?php echo htmlentities($row['location']); ?></h1>
                                            <h4><?php echo htmlentities($row['title_es']); ?> • <?php echo htmlentities(date('j/m/Y', strtotime($row['date_added']))); ?></h4>
                                            <hr>
                                            <p><?php echo htmlentities(substr($row['article_es'] , 0, 256));?>...</p>
                                        <?php } else if ($_GET['lang'] == 'en') { ?>
                                            <h1><?php echo htmlentities($row['location']); ?></h1>
                                            <h4><?php echo htmlentities($row['title_en']); ?> • <?php echo htmlentities(date('F j, Y', strtotime($row['date_added']))); ?></h4>
                                            <hr>
                                            <p><?php echo htmlentities(substr($row['article_en'] , 0, 256));?>...</p>
                                        <?php } else { ?>
                                            <h1><?php echo htmlentities($row['location']); ?></h1>
                                            <h4><?php echo htmlentities($row['title_es']); ?> • <?php echo htmlentities(date('j/m/Y', strtotime($row['date_added']))); ?></h4>
                                            <hr>
                                            <p><?php echo htmlentities(substr($row['article_es'] , 0, 256));?>...</p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </a>    
                    </div>
                </div>
            </div>
        </div>  
        <?php } ?>
    </div>

    <!-- Pagination -->
    <div class="container text-center">
        <div class="container">
            <!-- Verify there are coils -->
            <?php if (Article::getNumericalNumberOfPosts() > 10) { ?>
                <div class="pagination">
                    <!-- If page is eqaul to first don't display last button -->
                    <?php if ($_GET['page'] > 1) { ?>
                    <li><a href="<?php echo Utilities::appendVariableToUrl("page", $_GET['page'] - 1); ?>"><span class="glyphicon glyphicon-menu-left"></span></a></li>
                    <?php } ?>
                    <!-- List numerical pages -->
                    <?php for($x = 1; $x <= Article::listPagination($_GET['page']); $x++) : ?>
                        <li><a href="<?php echo Utilities::appendVariableToUrl("page", $x); ?>"<?php if($_GET['page'] == $x) {echo htmlspecialchars("class=selected");}?>><?php echo htmlspecialchars($x); ?></a></li>
                    <?php endfor; ?>
                    <!-- If page is eqaul to last don't display next button -->
                    <?php if ($_GET['page'] < Article::listPagination($_GET['page'])) { ?>
                    <li><a href="<?php echo Utilities::appendVariableToUrl("page", $_GET['page'] + 1); ?>"><span class="glyphicon glyphicon-menu-right"></span></a></li>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <!-- Footer -->
    <?php require "templates/footer.php"; ?>
</body>
</html>